source 'https://rubygems.org'

# Base system
gem 'rails', '4.2.4'
gem 'pg'

# Utilities
gem 'jquery-rails'
gem 'jbuilder', '~> 2.0'
gem 'turbolinks'
gem 'sdoc', '~> 0.4.0', group: :doc
gem 'bootstrap-sass'
gem 'autoprefixer-rails'

# Transpilers
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.1.0'
gem 'haml-rails'

# Components
gem 'devise'
gem 'recaptcha', '~> 1.1', require: "recaptcha/rails"

# Deployment
gem 'capistrano', '~> 3.4.0'
gem 'capistrano-uberspace', github: 'tessi/capistrano-uberspace'

group :development do
  gem 'rails_layout'
  gem 'web-console', '~> 2.0'
  gem 'spring'
end

group :test do
  gem "faker"
  gem "capybara"
  gem "database_cleaner"
  gem "launchy"
  gem "selenium-webdriver"
end

group :development, :test do
  gem 'pry-byebug'
  gem 'pry-stack_explorer'
  gem 'pry-rails'

  gem 'rspec-rails'
  gem 'factory_girl_rails'
end
