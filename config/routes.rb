Rails.application.routes.draw do
  devise_for :users

  root to: 'landingpage#index'
  get 'landingpage/index'
end
