set :application, 'fossbuzz-staging'
set :stage, :staging
set :rails_env, :staging

server 'alphard.uberspace.de',
  user: 'fossbuzz',
  roles: [:app, :web, :cron, :db],
  primary: true,
  ssh_options: {
    keys: %w{~/.ssh/fossbuzz},
    forward_agent: true,
    auth_methods: %w(publickey)
  }

set :user, 'fossbuzz'
set :branch, :staging
set :domain, 'staging.foss.buzz'
set :add_www_domain, false

set :deploy_to, '/home/fossbuzz/staging'
