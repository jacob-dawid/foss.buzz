set :application, 'fossbuzz-production'
set :stage, :production
set :rails_env, :production

server 'alphard.uberspace.de',
  user: 'fossbuzz',
  roles: [:app, :web, :cron, :db],
  primary: true,
  ssh_options: {
    keys: %w{~/.ssh/fossbuzz},
    forward_agent: true,
    auth_methods: %w(publickey)
  }

set :user, 'fossbuzz'
set :branch, :production
set :domain, 'foss.buzz'
set :add_www_domain, true

set :deploy_to, '/home/fossbuzz/production'
